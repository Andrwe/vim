" ino indentation
if exists('b:did_indent')
  finish
endif

setlocal tabstop=4
setlocal expandtab
setlocal shiftwidth=4
setlocal softtabstop=4
