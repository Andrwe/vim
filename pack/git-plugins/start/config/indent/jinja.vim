" jinja indentation
if exists('b:did_indent')
  finish
endif

setlocal tabstop=2
setlocal expandtab
setlocal shiftwidth=2
setlocal softtabstop=2
