" markdown indentation
if exists('b:did_indent')
  finish
endif

setlocal tabstop=3
setlocal expandtab
setlocal shiftwidth=3
setlocal softtabstop=3
