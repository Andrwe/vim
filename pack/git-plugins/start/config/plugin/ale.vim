map <F12> :ALEDetail<CR>
nn <silent> <M-d> :LspDefinition<cr>
nn <silent> <M-r> :LspReferences<cr>
nn <silent> <M-a> :LspWorkspaceSymbol<cr>
nn <silent> <M-l> :LspDocumentSymbol<cr>

let g:lsp_settings = {
\   "yaml-language-server": {
\     "workspace_config": {
\       "yaml": {
\         "customTags": [
\           "!reference sequence"
\         ]
\       }
\     }
\   }
\ }

function s:lsp_install_all_servers() abort
  let l:servers = [
        \ ["css", "vscode-css-language-server"],
        \ ["dockerfile", "docker-langserver"],
        \ ["html", "vscode-html-language-server"],
        \ ["javascript", "eslint-language-server"],
        \ ["json", "json-languageserver"],
        \ ["rst", "esbonio"],
        \ ["markdown", "marksman"],
        \ ["python", "pylsp"],
        \ ["python", "ruff-lsp"],
        \ ["sh", "bash-language-server"],
        \ ["yaml", "yaml-language-server"],
        \ ["ansible", "ansible-language-server"]
      \ ]
  for [l:ft, l:server] in l:servers
    :call lsp_settings#install_server(l:ft, l:server)
  endfor
endfunction

command! ServerInstallAll call s:lsp_install_all_servers()

" fix ale_complete
set completeopt=menu,menuone,preview,noselect,noinsert

" configure ale
let g:ale_completion_enabled = 1
let g:ale_set_loclist = 1
let g:ale_set_quickfix = 0
let g:ale_open_list = 1
let g:ale_close_preview_on_insert = 1
let g:ale_echo_cursor = 0
let g:ale_cursor_detail = 0
let g:ale_list_window_size = 5
" lint only when leaving insert mode
let g:ale_lint_on_insert_leave = 1
let g:ale_lint_on_text_changed = 'normal'
" configure airline
let g:airline#extensions#ale#enabled = 1
let g:ale_virtualenv_dir_names = ['./venv', '~/pyenv/default', '.env', '.venv', 'env', 'virtualenv', 'venv']
