filetype plugin indent on
syntax enable
colorscheme solarized8

set cursorline
set pastetoggle=<F10>
set background=dark
set autoindent
set showtabline=1
set modeline
set mouse=
set ttymouse=
set backspace=indent,eol,start
set number

let g:tex_flavor='latex'

if has('termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

if has("autocmd")
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Tell vim to remember certain things when we exit
"  '10 : marks will be remembered for up to 10 previously edited files
"  "100 : will save up to 100 lines for each register
"  :20 : up to 20 lines of command-line history will be remembered
"  % : saves and restores the buffer list
"  n... : where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.vim/info

" configure indent_guides
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_guide_size = 1

" gopass security
autocmd BufNewFile,BufRead /dev/shm/gopass.* setlocal noswapfile nobackup noundofile
autocmd BufNewFile,BufRead /private/**/gopass** setlocal noswapfile nobackup noundofile

map <F11> :lclose<CR>

"set wildmenu
"set wildoptions=pum
