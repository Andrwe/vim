# Vim Config

## Update Plugins

add following function to your bash-config and run it periodically:

```bash
function vim_upd() {
    local dir plugin
    printf "\e[32;1mupdate vim plugins\e[0m\n"
    for dir in "${HOME}/.vim/pack"/*/*/*/; do
        plugin="${dir%/}"
        plugin="${plugin##*/}"
        printf "\e[32;1m  %s\e[0m\n" "${plugin}"
        git -C "${dir}" pull --all
    done
    git -C "${HOME}/.vim" commit -sam "auto-update"
    git -C "${HOME}/.vim" push
}
```

## Install Dependencies

```bash
git clone https://gitlab.com/Andrwe/vim.git ~/.vim
ln -s ~/.vim/editorconfig ~/.editorconfig
python3 -m virtualenv ~/.venv/
~/.venv/bin/pip install "python-lsp-server[all]" yamllint proselint python-lsp-black
```

### Linux

```bash
~/.venv/bin/pip install nodeenv
~/.venv/bin/nodeenv ~/.nodeenv
~/.nodeenv/bin/npm -g install yaml-language-server bash-language-server
FZF=0.32.1; curl -L "https://github.com/junegunn/fzf/releases/download/${FZF}/fzf-${FZF}-linux_amd64.tar.gz" | tar -C ~/bin -xzf -
RIPGREP=13.0.0; curl -L "https://github.com/BurntSushi/ripgrep/releases/download/${RIPGREP}/ripgrep-${RIPGREP}-x86_64-unknown-linux-musl.tar.gz" | tar --strip-components=1 -C ~/bin -xzf - ripgrep-${RIPGREP}-x86_64-unknown-linux-musl/rg
```

### MacOS

```bash
brew install ansible-language-server yaml-language-server fzf ripgrep markdownlint-cli
```

### python-lsp Daemon

based on [Vim8 from morph027](https://gitlab.com/morph027/vim8)

#### SystemD

```bash
mkdir -p ~/.config/systemd/user
cat > ~/.config/systemd/user/pylsp.service <<EOF
[Unit]
Description=Python LSP Server - https://github.com/python-lsp/python-lsp-server

[Service]
ExecStart=%h/venv/bin/pylsp --tcp
Restart=on-failure

[Install]
WantedBy=default.target
EOF
systemctl --user enable --now pylsp.service
```

#### MacOS Launchd

```bash
mkdir -p ~/Library/LaunchAgents
cat > ~/Library/LaunchAgents/net.pylsp.server.plist <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
      <string>net.pylsp.server</string>
      <key>ProgramArguments</key>
      <array>
        <string>$HOME/.venv/bin/pylsp</string>
        <string>--tcp</string>
      </array>
      <key>RunAtLoad</key>
      <true/>
      <key>LaunchOnlyOnce</key>
      <true/>
      <key>KeepAlive</key>
      <true/>
   </dict>
</plist>
EOF
launchctl load Library/LaunchAgents/net.pylsp.server.plist
launchctl enable user/$(id -u $USER)/net.pylsp.server
launchctl start net.pylsp.server
```
